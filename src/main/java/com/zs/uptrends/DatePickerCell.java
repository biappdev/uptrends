package com.zs.uptrends;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableCell;

class DatePickerCell<S, T> extends TableCell<MonthDate, Date> {

    private DatePicker datePicker;
    private ObservableList<MonthDate> birthdayData;

    public DatePickerCell(ObservableList<MonthDate> listBirthdays) {

        super();

        this.birthdayData = listBirthdays;

        if (datePicker == null) {
            createDatePicker();
        }
        setGraphic(datePicker);
        setContentDisplay(ContentDisplay.GRAPHIC_ONLY);

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                datePicker.requestFocus();
            }
        });
    }

    @Override
    public void updateItem(Date item, boolean empty) {

        super.updateItem(item, empty);

        SimpleDateFormat smp = new SimpleDateFormat("dd/MM/yyyy");

        if (null == this.datePicker) {
            System.out.println("datePicker is NULL");
        }

        if (empty) {
            setText(null);
            setGraphic(null);
        } else {

            if (isEditing()) {
                setContentDisplay(ContentDisplay.TEXT_ONLY);

            } else {
                setDatepikerDate(smp.format(item));
                setText(smp.format(item));
                setGraphic(this.datePicker);
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            }
        }
    }

    private void setDatepikerDate(String dateAsStr) {

        LocalDate ld = null;
        int jour, mois, annee;

        jour = mois = annee = 0;
        try {
            jour = Integer.parseInt(dateAsStr.substring(0, 2));
            mois = Integer.parseInt(dateAsStr.substring(3, 5));
            annee = Integer.parseInt(dateAsStr.substring(6, dateAsStr.length()));
        } catch (NumberFormatException e) {
            System.out.println("setDatepikerDate / unexpected error " + e);
        }

        ld = LocalDate.of(annee, mois, jour);
        datePicker.setValue(ld);
    }

    private void createDatePicker() {
        this.datePicker = new DatePicker();
        datePicker.setPromptText("jj/mm/aaaa");
        datePicker.setEditable(true);

        datePicker.setOnAction(new EventHandler() {
            public void handle(Event t) {
                LocalDate date = datePicker.getValue();
                int index = getIndex();

                SimpleDateFormat smp = new SimpleDateFormat("dd/MM/yyyy");
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.DAY_OF_MONTH, date.getDayOfMonth());
                cal.set(Calendar.MONTH, date.getMonthValue() - 1);
                cal.set(Calendar.YEAR, date.getYear());

                setText(smp.format(cal.getTime()));
                commitEdit(cal.getTime());

                if (null != getBirthdayData()) {
                    getBirthdayData().get(index).setDate(cal.getTime());
                }
            }
        });

        setAlignment(Pos.CENTER);
    }

    @Override
    public void startEdit() {
        super.startEdit();
    }

    @Override
    public void cancelEdit() {
        super.cancelEdit();
        setContentDisplay(ContentDisplay.TEXT_ONLY);
    }

    public ObservableList<MonthDate> getBirthdayData() {
        return birthdayData;
    }

    public void setBirthdayData(ObservableList<MonthDate> birthdayData) {
        this.birthdayData = birthdayData;
    }

    public DatePicker getDatePicker() {
        return datePicker;
    }

    public void setDatePicker(DatePicker datePicker) {
        this.datePicker = datePicker;
    }

}





















//
//import javafx.beans.value.ChangeListener;
//import javafx.beans.value.ObservableValue;
//import javafx.beans.value.WritableValue;
//import javafx.collections.ObservableList;
//import javafx.event.EventHandler;
//import javafx.scene.control.*;
//import javafx.util.Callback;
//
//import java.time.LocalDate;
//
//public class DatePickerTableCell<T> extends TableCell<T, LocalDate> {
//
//    private final DatePicker datePicker;
//    private boolean listening = true;
//
//    // listener for changes in the datepicker
//    private final ChangeListener<LocalDate> listener = (observable, oldValue, newValue) -> {
//        if (listening) {
//            listening = false;
//
//            TableColumn<T, LocalDate> column = getTableColumn();
//            EventHandler<TableColumn.CellEditEvent<T, LocalDate>> handler = column.getOnEditCommit();
//            if (handler != null) {
//                // use TableColumn.onEditCommit if there is a handler
//                handler.handle(new TableColumn.CellEditEvent<>(
//                        (TableView<T>) getTableView(),
//                        new TablePosition<T, LocalDate>(getTableView(), getIndex(), column),
//                        TableColumn.<T, LocalDate>editCommitEvent(),
//                        newValue
//                ));
//            } else {
//                // otherwise check if ObservableValue from cellValueFactory is
//                // also writable and use in that case
//                ObservableValue<LocalDate> observableValue = column.getCellObservableValue((T) getTableRow().getItem());
//                if (observableValue instanceof WritableValue) {
//                    ((WritableValue) observableValue).setValue(newValue);
//                }
//            }
//
//            listening = true;
//        }
//    };
//
//    public DatePickerTableCell(ObservableList<MonthDate> dataList) {
//        this.datePicker = new DatePicker();
//        this.datePicker.valueProperty().addListener(listener);
//    }
//
//    @Override
//    protected void updateItem(LocalDate item, boolean empty) {
//        super.updateItem(item, empty);
//
//        if (empty) {
//            listening = false;
//            setGraphic(null);
//        } else {
//            listening = false;
//            setGraphic(this.datePicker);
//            this.datePicker.setValue(item);
//            listening = true;
//        }
//    }
//
////    public static <E> Callback<TableColumn<E, LocalDate>, TableCell<E, LocalDate>> forTableColumn() {
////        return column -> new DatePickerTableCell<>(dataList);
////    }
//
//}