package com.zs.uptrends;

import com.squareup.okhttp.*;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;


import javafx.stage.Stage;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;


import javax.net.ssl.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

public class Main extends  Application {

    public static ObservableList<MonthDate> dataList  = FXCollections.observableArrayList () ;

   public static ObservableList<String> groupName  = FXCollections.observableArrayList();
   public static  HashMap<String,String> nameGUId = new HashMap<>();

   public String [] month = {"Jan" , "Feb" , "Mar" , "April" , "May" , "June" , "July", "Aug", "Sep", "Oct" , "Nov" , "Dec" } ;

   public static  FileCreation fc = new FileCreation() ;

   public static File filepath = FileCreation.fileCreation();

   public static OkHttpClient client;

    public Main() {
    }

    public static  void main (String args [] ) throws IOException, KeyStoreException, CertificateException, NoSuchAlgorithmException, KeyManagementException {



//        ClassLoader loader = Thread.currentThread().getContextClassLoader();
//
//        //String CA_FILE = String.valueOf(getClass().getResource("api.uptrends.com.crt"));
//
//        InputStream fis = loader.getResourceAsStream("api.uptrends.com.crt");
//
//
//        X509Certificate ca = (X509Certificate) CertificateFactory.getInstance("X.509")
//                .generateCertificate(new BufferedInputStream(fis));
//
//        KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
//        ks.load(null, null);
//        ks.setCertificateEntry(Integer.toString(1), ca);
//
//        TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
//        tmf.init(ks);
//
//        SSLContext context = SSLContext.getInstance("TLS");
//        context.init(null, tmf.getTrustManagers(), null);



//        FileInputStream fis = new FileInputStream(CA_FILE);
//      C  X509Certificate ca = (X509Certificate) CertificateFactory.getInstance("X.509")
//                .generateCertificate(new BufferedInputStream(fis));

        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager(){
            public X509Certificate[] getAcceptedIssuers(){return null;}
            public void checkClientTrusted(X509Certificate[] certs, String authType){}
            public void checkServerTrusted(X509Certificate[] certs, String authType){}
        }};

// Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            ;
        }



//        disableSslVerification();



//
//        TrustManager[] trustAllCerts = new TrustManager[]{
//                new X509TrustManager() {
//                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
//                        return null;
//                    }
//                    public void checkClientTrusted(
//                            java.security.cert.X509Certificate[] certs, String authType) {
//                    }
//                    public void checkServerTrusted(
//                            java.security.cert.X509Certificate[] certs, String authType) {
//                    }
//                }};
//
//        try {
//            SSLContext sc = SSLContext.getInstance("SSL");
//            sc.init(null, trustAllCerts, new java.security.SecureRandom());
//            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
//        } catch (Exception e) {
//        }
//
//        // Create all-trusting host name verifier
//        HostnameVerifier allHostsValid = new HostnameVerifier() {
//            public boolean verify(String hostname, SSLSession session) {
//                return true;
//            }
//        };



        Locale.setDefault(Locale.US);

        launch(args);

    }


    public static void disableSslVerification() {
        try {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {

                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }
                    public void checkClientTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }
                    public void checkServerTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }
                }};

            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };

            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }

    }

    public static void addMaintaince(String groupID, String username, String password, Date date ,String startTime, String endTime ) throws ParseException, IOException {

        String encoding = Base64.getEncoder().encodeToString((username + ":" + password).getBytes(StandardCharsets.UTF_8));

        OkHttpClient client = new OkHttpClient();



        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateStr = formatter.format(date);


//      DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); //If you need time just put specific format for time like 'HH:mm:ss'
//
//
        String startDateTime = dateStr  + "T" + startTime + "Z";
        System.out.println("startDate"+startDateTime);

        String endDateTime = dateStr  + "T" + endTime + "Z";
        System.out.println("startDate"+endDateTime);

//        System.out.println( DateFormat.getDateInstance(DateFormat.SHORT).format(date) );

//        System.out.println(dateStr);

        MediaType mediaType = MediaType.parse("application/json");
//        RequestBody body = RequestBody.create(mediaType,
//                "{\r\n  \"Id\": 0," +
//                        "\r\n  \"ScheduleMode\": \"OneTime\"," +
//                        "\r\n  \"StartDateTime\": "+startDateTime+"," +
//                        "\r\n  \"EndDateTime\": "+endDateTime+"," +
////                        "\r\n  \"EndDateTime\": \"2021-01-30T18:30:11.495Z\"," +
//                        "\r\n  \"MaintenanceType\": \"DisableMonitoring\"\r\n}");





        JSONObject item = new JSONObject();
        item.put("Id", "0");
        item.put("ScheduleMode", "OneTime");
        item.put("StartDateTime", startDateTime);
        item.put("EndDateTime", endDateTime);
        item.put("MaintenanceType","DisableMonitoring");


//        RequestBody body = RequestBody.create(mediaType,
//                "{\r\n  \"Id\": 0," +
//                        "\r\n  \"ScheduleMode\": \"OneTime\"," +
//                        "\r\n  \"StartDateTime\": "+startDateTime+"," +
//                        "\r\n  \"EndDateTime\": "+endDateTime+"," +
//                        "\r\n  \"MaintenanceType\": \"DisableMonitoring\"\r\n}");
//
        URL url = null;
        try {
            url = new URL("https://api.uptrends.com/v4/MonitorGroup/"+groupID+"/AddMaintenancePeriodToAllMembers");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }


        System.out.println("url"+url);

        HttpURLConnection con =
                null;
        try {
            con = (HttpURLConnection) (url).openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // set the request method and properties.
        try {
            con.setRequestMethod("GET");
            con.setRequestProperty("Authorization", "Basic "+encoding);
            con.setRequestProperty("Content-Type", "application/json");
            con.setDoOutput(true);
        } catch (ProtocolException e) {
            e.printStackTrace();
        }





        try {
            // POST
            OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream());
            writer.write(String.valueOf(item));
            writer.flush();

            writer.close();


        } catch (Exception e) {
            e.printStackTrace();
        }
        int responseCode = con.getResponseCode();
        System.out.println("POST Response Code :: " + responseCode);

        if (responseCode == 204) { //success
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            // print result
            System.out.println(response.toString());
        } else {
            System.out.println("Downtime not added");
        }

//
//        try(BufferedReader br = new BufferedReader(
//                new InputStreamReader(con.getInputStream(), "utf-8"))) {
//            StringBuilder response = new StringBuilder();
//            String responseLine = null;
//            while ((responseLine = br.readLine()) != null) {
//                response.append(responseLine.trim());
//            }
//            System.out.println(response.toString());
//        }

        //////////////////////////////////////////////

//
//        HttpURLConnection con = null;

//
//        RequestBody body = RequestBody.create(mediaType , String.valueOf(item));
//        //                        "\r\n  \"EndDateTime\": \"2021-01-30T18:30:11.495Z\"," +
//
//        Request request = new Request.Builder()
//                .url("https://api.uptrends.com/v4/MonitorGroup/"+groupID+"/AddMaintenancePeriodToAllMembers")
//                .method("POST", body)
//                .addHeader("Authorization", "Basic "+encoding)
//                .addHeader("Content-Type", "application/json")
//                .build();
//
//        System.out.println();
//
//        Response response = null;
//        try {
//            response = client.newCall(request).execute();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        try {
//            System.out.println(response.body().string());
//            System.out.println(response.code());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        String status  =  "";
//
//        if (response.code() == 204 ){
//            status = "success";
//        }else {
//            status = "fail";
//        }

        String stringToFile = groupID + " " + startDateTime + " " + endDateTime + " " + responseCode ;

       fc.writeInFile(filepath, stringToFile + "\n");


    }


    public static void getGroupMonitorId(String username, String password) throws IOException {

        String encoding = Base64.getEncoder().encodeToString((username + ":" + password).getBytes(StandardCharsets.UTF_8));


//        System.out.println(username+password);

        URL url = null;
        try {
            url = new URL("https://api.uptrends.com/v4/MonitorGroup");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        HttpURLConnection con =
                null;
        try {
            con = (HttpURLConnection) (url).openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }


        // set the request method and properties.
        try {
            con.setRequestMethod("GET");
            con.setRequestProperty("Authorization", "Basic "+encoding);
            con.setRequestProperty("Content-Type", "application/json");
        } catch (ProtocolException e) {
            e.printStackTrace();
        }


        if (con.getResponseCode() != 401){
        

            InputStream ip = con.getInputStream();
        BufferedReader br1 =
        new BufferedReader(new InputStreamReader(ip));

        
//            System.out.println("Response Code:"
//                    + con.getResponseCode());

            JSONArray event = null;
            JSONObject responseJson = new JSONObject();
            JSONParser parser = new JSONParser();

        try {
            String t  = br1.lines().collect(Collectors.joining());;
            event = new JSONArray (t);
//            event = (JSONObject) parser.parse(br1);
//           System.out.println(event);

        } catch (Exception e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        String monitorGrpId = "";


    try {
        for (int n = 0; n < event.length(); n++) {

                JSONObject object = event.getJSONObject(n);
                groupName.add(String.valueOf(object.get("Description")));
                nameGUId.put(object.get("Description").toString(), object.get("MonitorGroupGuid").toString());
        }

//        if (nameGUId.isEmpty() && groupName.isEmpty()){
//            //Creating a dialog
//            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
//            //Setting the title
//            alert.setTitle("Alert");
//
//            //Setting the content of the dialog
//            alert.setContentText("Please enter correct Secret or Access key");
//            //Adding buttons to the dialog pane
//            alert.show();
//        }

    } catch ( Exception e) {

    }
        }else {

            Alert alert = new Alert(Alert.AlertType.WARNING);
            //Setting the title
            alert.setTitle("Alert");

            //Setting the content of the dialog
            alert.setContentText("Please enter correct Secret or Access key");
            //Adding buttons to the dialog pane
            alert.show();
        }

//    else {
//            //Creating a dialog
//            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
//            //Setting the title
//            alert.setTitle("Alert");
//
//            //Setting the content of the dialog
//            alert.setContentText("Please enter correct Secret or Access key");
//            //Adding buttons to the dialog pane
//            alert.show();
//        }
    }

    @Override
    public void start(Stage stage) throws Exception {

        stage.setTitle("Maintenance DownTime Tool");  //top header
        stage.show();

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.TOP_CENTER);
        grid.setHgap(10);
        grid.setVgap(15);
        grid.setPadding(new Insets(25, 25, 25, 25));


        GridPane grid1 = new GridPane();
        grid1.setAlignment(Pos.BOTTOM_CENTER);
        grid1.setHgap(10);
        grid1.setVgap(15);
        grid1.setPadding(new Insets(25, 25, 25, 25));

        Label apikeyLabel111 = new Label("API Key111");
        grid1.add( apikeyLabel111, 0, 5);

        Scene scene = new Scene(grid, 700, 900);

        stage.setScene(scene);



        ToggleGroup tg = new ToggleGroup();
//        A radio button with an empty string for its label
        RadioButton firstSunday = new RadioButton("1st Sunday");
        RadioButton secondSunday = new RadioButton("2nd Sunday");
        RadioButton thirdSunday= new RadioButton("3rd Sunday");
        RadioButton secondLastSunday = new RadioButton("Second Last Sunday");
        RadioButton lastSunday = new RadioButton("Last Sunday");

        firstSunday.setToggleGroup(tg);
        secondSunday.setToggleGroup(tg);
        thirdSunday.setToggleGroup(tg);
        secondLastSunday.setToggleGroup(tg);
        lastSunday.setToggleGroup(tg);


        Label radioLabel = new Label("Please select the recurrence pattern (Sunday) to add the Downtime");

//        grid.addRow(0, radioLabel);

        grid.add(radioLabel,0,0,2, 1);

//        Double abc = Double.valueOf(10);
//        Double abc = Double.valueOf(10);
//        grid.setHgap(50.5);

        grid.add(firstSunday,0,1,1,1);

        grid.add(secondSunday,1,1,1,1);

//        Region middle = new Region();
//        middle.setMinHeight(80);
//        middle.setStyle("-fx-background-color: green ;");

//        grid.addRow(1, firstSunday,middle,secondSunday);

//        grid.add(secondSunday,1,1,1,1);

        grid.add(thirdSunday,0,2);

        HBox hBox = new HBox(4,secondLastSunday,lastSunday);

//        GridPane.setConstraints(hBox,1 , 2);
//
//        grid.add(fourthSunday,1,2,1,1);
//        grid.add(lastSunday,  1,3,1,1);

        grid.add(hBox,1,2);
//        grid.add(gr);

//        grid.add(table,0,11,2,1);

        Label apikeyLabel = new Label("API Key");
        grid.add( apikeyLabel, 0, 4);

        TextField apikeyTF = new TextField();
        grid.add(apikeyTF, 1, 4);


        Label secretKeyLabel = new Label("Secret Key");
        grid.add( secretKeyLabel, 0, 5);

        TextField secretKeyTF = new TextField();
        grid.add(secretKeyTF, 1, 5);


        Label groupNameLabel = new Label("Group Name");
        grid.add( groupNameLabel, 0, 6);

        final ComboBox groupNamecomboBox = new ComboBox(groupName);

        grid.add(groupNamecomboBox,1,6);

        groupNamecomboBox.addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {


                if ( !apikeyTF.getText().isEmpty() && !secretKeyTF.getText().isEmpty() ) {

                    if (nameGUId.isEmpty() && groupName.isEmpty()) {
                        System.out.println("abc");

                        try {
                            System.out.println(apikeyTF.getText() + secretKeyTF.getText());
                            getGroupMonitorId(apikeyTF.getText(), secretKeyTF.getText());


                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }else {
                    System.out.println("abcd");
                    //Creating a dialog
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    //Setting the title
                    alert.setTitle("Alert");
//                    ButtonType type = new ButtonType("Ok", ButtonBar.ButtonData.OK_DONE);
                    //Setting the content of the dialog
                    alert.setContentText("Please enter Access Key and Secret Key");
                    //Adding buttons to the dialog pane
//                    alert.getDialogPane().getButtonTypes().add(type);

                    alert.show();
                }
            }
        });

        Label startTimeLbl = new Label("Start Time");
        grid.add( startTimeLbl, 0, 8);


        SpinnerValueFactory<LocalTime> factory = new SpinnerValueFactory<LocalTime>() {

            {
                setValue(defaultValue());
            }

            private LocalTime defaultValue() {
                return LocalTime.parse("07:00");
                //LocalTime.now().truncatedTo(ChronoUnit.HOURS);
            }

            @Override
            public void decrement(int steps) {
                LocalTime value = getValue();
                setValue(value == null ? defaultValue() : value.minusMinutes(30));
            }

            @Override
            public void increment(int steps) {
                LocalTime value = getValue();
                setValue(value == null ? defaultValue() : value.minusMinutes(30));
            }

        };

        Spinner<LocalTime> spinner = new Spinner<>();
        spinner.setValueFactory(factory);
        grid.add(spinner,1,8);


        Label IST1 = new Label("                                                 GMT");
        grid.add(IST1,1,8);


        Label endTimeLbl = new Label("End Time");
        grid.add( endTimeLbl, 0, 9);



        SpinnerValueFactory<LocalTime> factory1 = new SpinnerValueFactory<LocalTime>() {

            {
                setValue(defaultValue());
            }

            private LocalTime defaultValue() {
//                return LocalTime.now().truncatedTo(ChronoUnit.HOURS);
                return LocalTime.parse("19:00");

            }

            @Override
            public void decrement(int steps) {
                LocalTime value = getValue();
                setValue(value == null ? defaultValue() : value.minusMinutes(30));
            }

            @Override
            public void increment(int steps) {
                LocalTime value = getValue();
                setValue(value == null ? defaultValue() : value.minusMinutes(30));
            }

        };

        Label IST2 = new Label("                                                 GMT");
        grid.add(IST2,1,9);

        Spinner<LocalTime> spinner1 = new Spinner<>();
        spinner1.setValueFactory(factory1);
        grid.add( spinner1 ,1,9);

        Button btn = new Button("Add Downtime");
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        hbBtn.getChildren().add(btn);
        grid.add(hbBtn, 1, 13);

        Label dateLbl  = new Label();
        grid.add(dateLbl,0,11);

        int year = Calendar.getInstance().get(Calendar.YEAR);

        System.out.println(year);


        tg.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {

                if (firstSunday.isSelected()) {

                    dataList.clear();

                    ArrayList dateArray = FirstSunday.getFirstSundayYear(year);
                    addDateinTable(year,month,dateArray);

                }
                else if (secondSunday.isSelected()){

                    dataList.clear();

                    ArrayList dateArray = SecondSunday.getSecondSundayYear(year);
                    addDateinTable(year,month,dateArray);

                }
                else  if (thirdSunday.isSelected()) {

                    dataList.clear();


                    ArrayList dateArray = ThirdSunday.getThirdSundayYear(year);
                    addDateinTable(year,month,dateArray);

                }
                else if (secondLastSunday.isSelected()) {
                    dataList.clear();

                    ArrayList dateArray = SecondLastSunday.getSecodLastSundayYear(year);
                    addDateinTable(year,month,dateArray);
                }
                else if (lastSunday.isSelected()){

                    dataList.clear();

                    ArrayList dateArray = LastSunday.getLastSundayYear(year);
                    addDateinTable(year,month,dateArray);
                }


            }
        });


        TableView table = new TableView();
//        table.setEditable(true);

        table.setPrefHeight(500);

        TableColumn nameColumn = new TableColumn("Month");
        nameColumn.setPrefWidth(100);

        nameColumn.setCellValueFactory(
                new PropertyValueFactory<MonthDate, String>("name"));
//        nameColumn.setEditable(true);


        TableColumn dateColumn = new TableColumn("Date");
//        dateColumn.setEditable(true);

        TableColumn removeButton = new TableColumn("Add Action");

        nameColumn.setSortable(false);
        dateColumn.setSortable(false);
        removeButton.setSortable(false);

        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        javafx.util.Callback<TableColumn<MonthDate, String>, TableCell<MonthDate, String>> cellFactory
                =
                new javafx.util.Callback<TableColumn<MonthDate, String>, TableCell<MonthDate, String>>() {
                    @Override
                    public TableCell call(final TableColumn<MonthDate, String> param) {
                        final TableCell<MonthDate, String> cell = new TableCell<MonthDate, String>() {

                            final Button btn = new Button("Delete");

                            @Override
                            public void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                if (empty) {
                                    setGraphic(null);
                                    setText(null);
                                } else {
                                    btn.setOnAction(event -> {

                                       MonthDate monthDate = getTableView().getItems().get(getIndex());

                                        dataList.remove(getIndex());

                                       System.out.println(monthDate.getDate()+ "" + monthDate.getName());

//                                        Person person = getTableView().getItems().get(getIndex());
//                                        System.out.println(person.getFirstName()
//                                                + "   " + person.getLastName());
                                    });
                                    setGraphic(btn);
                                    setText(null);
                                }
                            }
                        };
                        return cell;
                    }
                };

        removeButton.setCellFactory(cellFactory);

//        dateColumn.setPrefWidth(200);
        nameColumn.setMinWidth(50);
        dateColumn.setMinWidth(190);
        removeButton.setMinWidth(60);

        dateColumn.setCellValueFactory(new PropertyValueFactory<MonthDate, Date>("date"));

        dateColumn.setCellFactory(new javafx.util.Callback<TableColumn, TableCell>() {
            @Override
            public TableCell call(TableColumn param) {


                DatePickerCell datePick = new DatePickerCell (dataList);
                return datePick;
//                return null;
            }
        });

        table.getColumns().addAll(nameColumn, dateColumn, removeButton);
        table.setItems(dataList);
        table.setItems(dataList);


        Label tableLabel = new Label("Schedule for the Downtime Maintenance");

        grid.add(tableLabel,0,11,2,1);


        grid.add(table,0,12,2,1);


        Button addData = new Button("Add Date");

        grid.add(addData , 3 , 12);


        addData.setOnAction( event ->  {

            CustomPopUp customPopUp = new CustomPopUp();
            customPopUp.popDisplay(dataList);


        });


        btn.setOnAction( event ->  {



//            CustomProgressBar.progressBar1();
//                CustomProgressBar.main();

            String apikey =  apikeyTF.getText();
            String secretKey = secretKeyTF.getText();

//            String groupName = groupNameTF.getText();

            String startTime = String.valueOf(spinner.getValue());

            startTime = startTime + ":00.000";

            String endTime = String.valueOf(spinner1.getValue());

            endTime = endTime + ":00.000";

            String groupName1 = "";
            try {
                groupName1 = groupNamecomboBox.getSelectionModel().getSelectedItem().toString();
            }catch ( Exception e){

            }
            String groupN   =  nameGUId.get(groupName1);

            Alert alert = new Alert(Alert.AlertType.NONE);

            if (apikey.isEmpty() || secretKey.isEmpty() || dataList.isEmpty()  ){

                alert.setAlertType(Alert.AlertType.WARNING);

                // set content text
                alert.setContentText("Please Enter the required details ");

                // show the dialog
                alert.showAndWait();

            }else {
                Alert alertDia = new Alert(Alert.AlertType.INFORMATION);
                alertDia.setHeaderText("Wait");
                alertDia.setContentText("Adding Downtime");

//                alertDia.showAndWait();

                alertDia.show();
//                alertDia.showAndWait();
                //functions to be called
                try {
//                    CustomProgressBar.progressBar1();
//
//                    float f = (float) (100.0/dataList.size());

                    for (int i = 0 ; i < dataList.size() ; i++) {

                        System.out.println("groupN"+groupN);
                        addMaintaince(groupN, apikey, secretKey, dataList.get(i).getDate(), startTime, endTime);
//                        CustomProgressBar.fill((i+1)*f);
                    }

                    alertDia.close();


                } catch (ParseException | IOException e) {
                    e.printStackTrace();
                }
                alertDia.close();
            }

        });


    }



    public  static void addDateinTable ( int year , String [] month , ArrayList dateArray1)  {

        for ( int i =0 ; i < dateArray1.size() ; i++){

            try {

                System.out.println("add in dataList");

                Main main = new Main();

                System.out.println(main.dataList.size() + " " + month[i] + " " + dateToString((String) dateArray1.get(i)));

                main.dataList.add(i, new MonthDate( month[i] , dateToString((String) dateArray1.get(i))) );


            }catch ( Exception e){
//                System.out.println(e.toString());
            }
        }

    }

    public  static  Date dateToString (String str) throws ParseException {
        Date date1=new SimpleDateFormat("yyyy-MM-dd").parse(str);

        return  date1;

    }





}
