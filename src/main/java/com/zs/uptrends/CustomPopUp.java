package com.zs.uptrends;

import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Pair;

import java.text.ParseException;
import java.util.Date;

public class CustomPopUp {

    public void popDisplay(ObservableList<MonthDate> dataList) {

        Dialog<Pair<String, String>> dialog = new Dialog<>();
        dialog.setTitle("Add Date");
        dialog.setHeaderText("Please Add Downtime Date");

        ButtonType loginButtonType = new ButtonType("OK", ButtonBar.ButtonData.OK_DONE);

        Button okButton = new Button("..OK..");
        Button cancelButton = new Button("CANCEL");

        dialog.getDialogPane().getButtonTypes().addAll( ButtonType.CANCEL);

        GridPane gridPane = new GridPane();
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(20, 150, 10, 10));

        Label dateLabel = new Label("Select Date");

        DatePicker dp = new DatePicker();

        gridPane.add(dateLabel,0,1);
        gridPane.add(dp, 1,1);

        gridPane.add(okButton,1,2);
//        gridPane.add(cancelButton,2,2);

        dialog.getDialogPane().setContent(gridPane);

        dialog.show();


//    dialog.setResultConverter( dialogButton -> {
//
//    if (dialogButton == loginButtonType )
//
//    });

        okButton.setOnAction( event -> {

            String str = dp.getValue().toString();

            System.out.println(str);
//

            Date currentMonth = null;
            try {
                currentMonth = Main.dateToString(str);


            } catch (ParseException e) {
                e.printStackTrace();
            }

            Main main = new Main();

            try {
                dataList.add( dataList.size() ,new MonthDate( main.month[currentMonth.getMonth()] , Main.dateToString(str) ));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            dialog.close();
//
//            for (int i =0 ; i < dataList.size() ; i++ ){
//
//                System.out.println(dataList.get(i).getDate());
//            }

        });
    }
}
