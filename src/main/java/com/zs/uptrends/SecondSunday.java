package com.zs.uptrends;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class SecondSunday {

    public static ArrayList<String> dateArrayStg = new ArrayList<>();

    public static String getSecondSunday(int year, int month) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        cal.set(Calendar.DAY_OF_WEEK_IN_MONTH, 1);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.YEAR, year);

        SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new GregorianCalendar(year, month , cal.get(Calendar.DATE)+7).getTime();

        System.out.println(date);
        return newFormat.format(date);


    }

    public static ArrayList getSecondSundayYear(int year ) {
//        System.out.println( getThirdSunday(year, month) );

//        return getThirdSunday(year,month);


        for (int i =0 ; i < 12 ; i++){

//            getLastSunday(i,2020);
            dateArrayStg.add(getSecondSunday(year, i) );
//            System.out.println(date);
        }

        System.out.println("STG Dates");
        for (int i = 0; i < dateArrayStg.size(); i++) {
            System.out.println(dateArrayStg.get(i) + " ");
        }

        return dateArrayStg;
    }


}
