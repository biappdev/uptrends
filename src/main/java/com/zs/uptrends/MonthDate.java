package com.zs.uptrends;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.StringProperty;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Date;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;


public class MonthDate {

    private final SimpleObjectProperty<Date> date;
    private final StringProperty name;

    public MonthDate(String name, Date date) {
        this.name = new SimpleStringProperty(name);
        this.date = new SimpleObjectProperty(date);
    }


    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public StringProperty nameProperty() {
        return name;
    }

    public Date getDate() {
        return (Date) date.get();
    }

    public String getDateAsString() {
        SimpleDateFormat smp = new SimpleDateFormat("dd MMMMM yyyy");
        String strDate = (null == date || null == date.get())
                ? "" : smp.format(date.get());

        return strDate;
    }

    public void setDate(Date date) {
        this.date.set(date);
    }



//    public String monthName;
//    public Date dayofMonth;
//
//
//    public MonthDate (String monthName, Date dayofMonth) {
//
//        this.monthName = monthName;
//        this.dayofMonth = dayofMonth;
//    }
//
//    public String getMonthName() {
//        return monthName;
//    }
//
//    public void setMonthName(String monthName) {
//        this.monthName = monthName;
//    }
//
//    public void setDayofMonth(Date dayofMonth) {
//        this.dayofMonth = dayofMonth;
//    }
//
//    public Date getDayofMonth() {
//        return dayofMonth;
//    }
}
