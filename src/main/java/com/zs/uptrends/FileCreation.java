package com.zs.uptrends;

import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileCreation {


    public static  void DirCreation(){

        File file = FileSystemView.getFileSystemView().getHomeDirectory();
        File file1 = new File( file , "Uptrends");

        System.out.println(file1);

        if (!file1.exists()) {
            System.out.println("creating directory: " + file1.getName());
            boolean result = false;

            try{
                file1.mkdir();
                result = true;
            }
            catch(SecurityException se){
                //handle it
            }
            if(result) {
                System.out.println("DIR created");
            }
        }
    }

    public static File fileCreation(){

        File logPath ;
        DirCreation();

        Date date = new Date();

        SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");

        File file = FileSystemView.getFileSystemView().getHomeDirectory();
        File file1 = new File( file , "Uptrends");
        logPath = new File(file1,  newFormat.format(date)+".txt");

        try {
            logPath.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return  logPath;

    }

    public  void writeInFile(File path, String str){

        try
        {

            FileWriter fw = new FileWriter(path,true); //the true will append the new data
            fw.write(str);//appends the string to the file
            fw.close();
        }
        catch(IOException ioe)
        {
            System.err.println("IOException: " + ioe.getMessage());
        }
    }
}
