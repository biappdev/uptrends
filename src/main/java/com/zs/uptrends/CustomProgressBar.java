package com.zs.uptrends;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CustomProgressBar extends JFrame {
    static JFrame f;

    static JProgressBar b;

    public  static  void progressBar1 () {

        f = new JFrame("Progress");

        // create a panel
        JPanel p = new JPanel();


        // create a progressbar
        b = new JProgressBar();

        b.setLocation(100,80);

        // set initial value
        b.setValue(0);

        b.setStringPainted(true);


        f.setLocation(280,300);


        // add progressbar
        p.add(b);


        JButton button = new JButton("Close");

        // Add button to JPanel
        p.add(button);

        button.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                //your actions
                f.setVisible(false);
            }
        });



        // add panel
        f.add(p);

        // set the size of the frame
        f.setSize(200, 100);
        f.setVisible(true);
//        f.show();



    }

    // function to increase progress
    public static void fill(float num)
    {
        try {

            for (int i = 0 ; i <= 100 ; i++){
//                Thread.sleep(10);
                b.setValue((int) num);
            }
        }
        catch (Exception e) {
        }
    }
}
