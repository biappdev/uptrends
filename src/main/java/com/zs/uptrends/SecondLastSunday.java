package com.zs.uptrends;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class SecondLastSunday {

    public static ArrayList<String> dateArrayStg = new ArrayList<>();

    public static String getSecondLastSunday(int year, int month) {
        Calendar cal = Calendar.getInstance();
        cal.set( year, month + 1, 1 );
        cal.add(Calendar.DATE, -1);
        cal.add( Calendar.DAY_OF_MONTH, -( cal.get( Calendar.DAY_OF_WEEK ) - 1 ) );
//        return cal.getTime();
        SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new GregorianCalendar(year, month , cal.get(Calendar.DATE) - 7).getTime();

        return newFormat.format(date);

    }

    public static ArrayList getSecodLastSundayYear(int year ) {
//        System.out.println( getThirdSunday(year, month) );

//        return getThirdSunday(year,month);


        for (int i =0 ; i < 12 ; i++){

//            getLastSunday(i,2020);
            dateArrayStg.add(getSecondLastSunday(year, i) );
//            System.out.println(date);
        }

        System.out.println("STG Dates");
        for (int i = 0; i < dateArrayStg.size(); i++) {
            System.out.println(dateArrayStg.get(i) + " ");
        }

        return dateArrayStg;
    }


}
