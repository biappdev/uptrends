package com.zs.uptrends;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class ThirdSunday {

        public static ArrayList<String> dateArrayPreProd = new ArrayList<>();

        public static String getThirdSunday(int month, int year) {
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
            cal.set(Calendar.DAY_OF_WEEK_IN_MONTH, 1);
            cal.set(Calendar.MONTH, month);
            cal.set(Calendar.YEAR, year);

            SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new GregorianCalendar(year, month , cal.get(Calendar.DATE)+14).getTime();

            return newFormat.format(date);

        }

        public static ArrayList getThirdSundayYear(int year) {

            for (int i =0 ; i < 12 ; i++){
                dateArrayPreProd.add(getThirdSunday(i, year) );
            }

            System.out.println("Dev Dates");

            for (int i = 0; i < dateArrayPreProd.size(); i++) {
                System.out.print(dateArrayPreProd.get(i) + " ");
            }

            return dateArrayPreProd;
        }
}
