package com.zs.uptrends;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class LastSunday {

    public static ArrayList<String> dateArrayPreProd = new ArrayList<>();


    public static String getLastSunday(int month, int year) {

        Calendar cal = Calendar.getInstance();
        cal.set( year, month + 1, 1 );
        cal.add(Calendar.DATE, -1);
        cal.add( Calendar.DAY_OF_MONTH, -( cal.get( Calendar.DAY_OF_WEEK ) - 1 ) );
//        return cal.getTime();
        SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new GregorianCalendar(year, month , cal.get(Calendar.DATE) ).getTime();

        return newFormat.format(date);

    }

    public static ArrayList getLastSundayYear(int year) {

        for (int i =0 ; i < 12 ; i++){
            dateArrayPreProd.add(getLastSunday(i, year) );
        }

        for (int i = 0; i < dateArrayPreProd.size(); i++) {
            System.out.print(dateArrayPreProd.get(i) + " ");
        }

        return dateArrayPreProd;
    }

}
